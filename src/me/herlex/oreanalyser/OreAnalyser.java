package me.herlex.oreanalyser;
     
import java.io.File;
import java.util.logging.Logger;
import net.milkbowl.vault.chat.Chat;
import net.milkbowl.vault.economy.Economy;
import net.milkbowl.vault.permission.Permission;
import org.bukkit.Bukkit;
import me.herlex.oreanalyser.commands.OACE;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

public class OreAnalyser extends JavaPlugin
{
    private File file;
    @SuppressWarnings("NonConstantLogger")
    public  Logger logger = Logger.getLogger("Minecraft");
    public static Economy econ = null;
    public static Permission perms = null;
    public static Chat chat = null;

    @Override
    public void onEnable()
    {
        PluginDescriptionFile pdFile = this.getDescription();
        logger.info(new StringBuilder(pdFile.getName()).append(" Version ").append(pdFile.getVersion()).append(" Has been Enabled!").toString());
        getCommand("oa").setExecutor(new OACE(this));      
    }
           
    @Override
    public void onDisable()
    {
        PluginDescriptionFile pdfFile = this.getDescription();
        Bukkit.getScheduler().cancelTasks(this);
        logger.info(new StringBuilder(pdfFile.getName()).append(" Has been Disabled!").toString());
    }
}


