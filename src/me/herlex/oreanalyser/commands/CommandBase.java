
package me.herlex.oreanalyser.commands;

import me.herlex.oreanalyser.OreAnalyser;

public class CommandBase 
{
    
    protected OreAnalyser main;
    
    protected CommandBase(OreAnalyser main)
    {
        this.main = main;
    }
    
}
