package me.herlex.oreanalyser.commands;

import java.util.ArrayList;
import java.util.Arrays;
import me.herlex.oreanalyser.OreAnalyser;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class OACE implements CommandExecutor
{
    protected final CommandHelp help;
    protected final CommandScan scan;
    
    
    public OACE(OreAnalyser main)
    {
        help = new CommandHelp(main);
        scan = new CommandScan(main);
    }
    
    public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args)
    {		

        Player player = (Player) sender;

        if(args.length < 1)
        {
            args = new String[]{"help"};
        }

        if (commandLabel.equalsIgnoreCase("oa")) 
        {
            String command = args[0];
            args = Arrays.copyOfRange(args, 1, args.length);
            
            if(command.equalsIgnoreCase("scan"))
            {
                if(args.length <1)
                {
                    player.sendMessage("Correct Usage: /oa scan <id|all> <radius>");
                }
                else
                {
                return scan.execute(player, args);
                }
            }
            if(command.equalsIgnoreCase("help"))
            {
                ArrayList<String> arguments = new ArrayList<String>(Arrays.asList(args));
                arguments.add(0, command);

                return help.execute(player, arguments.toArray(new String[0]));
            }

        }
        return false;
    }

}

