package me.herlex.oreanalyser.commands;

import me.herlex.oreanalyser.OreAnalyser;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

public class CommandScan extends CommandBase
{
    
    protected CommandScan(OreAnalyser main)
    {
        super(main);
    }
    public boolean execute(Player player, String[] args)
    {
        Location loc = player.getLocation();
        int locx = (int)loc.getX();
        int locz = (int)loc.getZ();
        World world = loc.getWorld();
        double erz = 0;
        double all = 0;
        int radius = Integer.parseInt(args[1]);
        
        player.sendMessage("[OreAnalyser] Start scanning...");
        for(int x = locx - radius; x < locx + radius; x++)
        {
            for(int y = 0; y < 256; y++)
            {
                for(int z = locz - radius; z < locz + radius; z++)
                {
                    Location blockloc = new Location(world, x, y, z);          
                    Block block = blockloc.getBlock();
                    if(block != null)
                    {
                        int blockid = block.getTypeId();
                        if(blockid != 0)
                            all++;
                        
                        if(args[0].equalsIgnoreCase("all"))
                        {
                            if(blockid == 14 || blockid == 15 || blockid == 16 || blockid == 15 || blockid == 21 || blockid == 56 || blockid == 73 || blockid == 74 || blockid == 129)
                                erz++;
                        }
                        else
                        {
                        int erzid = Integer.parseInt(args[0]);
                        if(erzid == 14 || erzid == 15 || erzid == 16 || erzid == 15 || erzid == 21 || erzid == 56 || erzid == 73 || erzid == 74 || erzid == 129)
                        {
                            if(erzid == blockid)
                                erz++;
                        }
                        else
                        {
                            player.sendMessage("Your selected ID isn't Ore!");
                            return true;
                        }
                        }
                    }
                }
            }
        }
        double percentage = (erz/all)*100;
        percentage = Math.round(percentage*100)/100.0;
        player.sendMessage("[OreAnalyser] Scanning complete!");
        player.sendMessage("[OreAnalyser] Blocks: " + all + " Ore: " + erz);
        player.sendMessage("[OreAnalyser] Percentage of Ore: " + percentage + "%");
        return true;
    }

}