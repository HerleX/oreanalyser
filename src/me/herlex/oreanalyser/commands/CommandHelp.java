package me.herlex.oreanalyser.commands;

import me.herlex.oreanalyser.OreAnalyser;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public class CommandHelp extends CommandBase
{
    
    protected CommandHelp(OreAnalyser main)
    {
        super(main);
    }
    
    public boolean execute(Player player, String[] args)
    {
        player.sendMessage(ChatColor.AQUA + "############OreAnalyser############");
        player.sendMessage(ChatColor.GRAY + "/oa  help" + ChatColor.WHITE + "- Opens this Help");
        player.sendMessage(ChatColor.GRAY + "/oa  scan <id|all> <radius>" + ChatColor.WHITE + "- Scan for Ore");    
        return true;
    }

}